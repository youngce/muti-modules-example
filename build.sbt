name := "muti-module"

version := "1.0"

scalaVersion := "2.12.0"

lazy val root = project.in(file(".")).aggregate(coreA, coreB, coreLibrary)

lazy val coreA = Project("CoreA", file("core-a"))//.setSbtFiles(file("coreA.sbt"))
  .settings(
  organization := "org.me",
  version := "0.1-SNAPSHOT"
)

lazy val coreB = Project("CoreB", file("core-b"))
//  .settings(
//  organization := "org.me",
//  libraryDependencies += "org.apache.kafka" %% "kafka" % "0.8.2-beta",
//  version := "0.3-SNAPSHOT"
//)

lazy val coreLibrary = Project("UberCore", file("core-main")).dependsOn(coreA, coreB).settings(
  organization := "org.me",
  version := "0.2-SNAPSHOT"
)
    